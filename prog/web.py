import time
from datetime import datetime
from flask import Flask, render_template, request, redirect, url_for

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)  # the pin numbers refer to the board connector not the chip
GPIO.setwarnings(False)
GPIO.setup(16, GPIO.IN, GPIO.PUD_UP) # set up pin ?? (one of the above listed pins) as an input with a pull-up resistor
GPIO.setup(18, GPIO.IN, GPIO.PUD_UP) # set up pin ?? (one of the above listed pins) as an input with a pull-up resistor
GPIO.setup(37, GPIO.OUT)
GPIO.output(37, GPIO.LOW)
GPIO.setup(38, GPIO.OUT)
GPIO.output(38, GPIO.LOW)
GPIO.setup(13, GPIO.OUT)
GPIO.output(13, GPIO.HIGH)
GPIO.setup(15, GPIO.OUT)
GPIO.output(15, GPIO.HIGH)




app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
        # if GPIO.input(16) == GPIO.HIGH and GPIO.input(18) == GPIO.HIGH and GPIO.input(13) == GPIO.HIGH and GPIO.input(15) == GPIO.HIGH:
        #      print("One of the Garages are Opening/Closing")
    return app.send_static_file('Question.html')
        #      return app.send_static_file('main.html')
        # else:  
        #      if GPIO.input(16) == GPIO.LOW and GPIO.input(13) == GPIO.LOW:
        #            print ("Garages are goth Closed")
        #            return app.send_static_file('Closed.html')
        #      if GPIO.input(18) == GPIO.LOW and GPIO(15).input == GPIO.LOW:
        #            print ("Garages are both Open")
        #            return app.send_static_file('Open.html')


@app.route('/Garage', methods=['GET', 'POST'])
def Garage():
        name = request.form['garagecode']
        if name == '26961':  # 12345678 is the Password that Opens Garage Door (Code if Password is Correct)
                GPIO.output(37, GPIO.HIGH)
                time.sleep(1)
                GPIO.output(37, GPIO.LOW)
                time.sleep(2)

                # if GPIO.input(16) == GPIO.HIGH and GPIO.input(18) == GPIO.HIGH:
                print("Large Garage is Opening/Closing")
                # return app.send_static_file('Question.html')
                return redirect(url_for('index'))
                # else:
                #   if GPIO.input(16) == GPIO.LOW:
                #         print ("Largfe Garage is Closed")
                #         return app.send_static_file('Closed.html')
                #   if GPIO.input(18) == GPIO.LOW:
                #         print ("Large Garage is Open")
                #         return app.send_static_file('Open.html')
        # 12345678 is the Password that Opens Garage Door (Code if Password is Correct)
        elif name == '26962':
                GPIO.output(38, GPIO.HIGH)
                time.sleep(1)
                GPIO.output(38, GPIO.LOW)
                time.sleep(2)

                # if GPIO.input(13) == GPIO.HIGH and GPIO.input(15) == GPIO.HIGH:
                print("Small Garage is Opening/Closing")
                # return app.send_static_file('Question.html')
                return redirect(url_for('index'))
                # else:
                #   if GPIO.input(13) == GPIO.LOW:
                #         print ("Small Garage is Closed")
                #         return app.send_static_file('Closed.html')
                #   if GPIO.input(15) == GPIO.LOW:
                #         print ("Small Garage is Open")
                #         return app.send_static_file('Open.html')

        if name != '26961' and name != '26962':  # 12345678 is the Password that Opens Garage Door (Code if Password is Incorrect)
                if name == "":
                        name = "NULL"
                        print("Garage Code Entered: " + name)
                        print("Nothing is going to happen without a code")
                # if GPIO.input(13) == GPIO.HIGH and GPIO.input(18) == GPIO.HIGH:
                # print("Garage is Opening/Closing")
                # return app.send_static_file('Question.html')
                return redirect(url_for('index'))
                # else:
                #   if GPIO.input(13) == GPIO.LOW:
                #         print ("Garage is Closed")
                #         return app.send_static_file('Closed.html')
                #   if GPIO.input(15) == GPIO.LOW:
                #         print ("Garage is Open")
                #         return app.send_static_file('Open.html')

@app.route('/stylesheet.css')
def stylesheet():
        return app.send_static_file('stylesheet.css')

@app.route('/Log')
def logfile():
        return app.send_static_file('log.txt')

@app.route('/images/<picture>')
def images(picture):
        return app.send_static_file('images/' + picture)

if __name__ == '__main__':
        app.run(debug=False, host='0.0.0.0', port=5000)
